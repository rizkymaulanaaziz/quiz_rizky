<!DOCTYPE html>
<html>
<head>
	<title>Kalkulator</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="css/js/script.js"></script>
</head>
<body>
<center>
<div class="container">
	<form class="kalku">
		<div class="angka">
			<input type="text" name="nilai" class="nilai"/>
		</div>
		<div class="cl_hs">
			<input type="reset" value="clear" class="clear" name="clear"/>
			<input type="text" name="hasil" class="hasil">
		</div>
		<div class="operand">
			<input type="button"  class="operation" value="+" onclick='tambah()'/>
			<input type="button" class="operation" value="-" onclick='kurang()'/>
			<input type="button" class="operation" value="/" onclick='bagi()'/>
			<input type="button"  class="operation" value="x" onclick='kali()'/>
			<input type="button"  class="samadengan" value="=" onclick='samadengan()'/>
		</div>
	</form>
</div>
</center>
</body>
</html>